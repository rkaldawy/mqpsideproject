from uuid import uuid4


class Graph:

    def __init__(self):
        self.nodes = {}

    def addRouter(self, ip):
        node = Router(ip)
        self.nodes[node.id] = node
        return node.id

    def addSwitch(self, mac):
        node = Switch(mac)
        self.nodes[node.id] = node
        return node.id

    def printGraph(self):
        for id, node in self.nodes.items():
            node.printNode()
            # node.printEdges()
            print()

    def addEdge(self, id_a, id_b, cost):
        A = self.findNode(id_a)
        B = self.findNode(id_b)

        if A is None or B is None:
            return 1

        A.addEdge(A, B, cost)

        return 0

    def findNode(self, id):
        try:
            node = self.nodes[id]
            return node
        except KeyError:
            return None

    def assignTags(self, id):
        A = self.findNode(id)
        if A is None:
            return

        A.assignTags()

class Edge:

    def __init__(self, src, dest, cost):
        self.src = src
        self.dest = dest
        self.cost = cost

class Node:

    def __init__(self):
        self.id = uuid4()
        self.edges = []
        self.tags = {}
        self.mark = 0

    def addEdge(self, src, dest, cost):
        e = Edge(src, dest, cost)
        self.edges += [e]

    def printEdges(self):
        print("Neighbors of " + self.getPrintableID() + ": ")
        for edge in self.edges:
            edge.dest.printNode()

    def printNode(self):
        print(self.getPrintableID())
        print("VLAN tags: " + " ".join(str(k) for k, v in self.tags.items()))

    def getPrintableID(self):
        pass

    def assignTags(self):
        c = Counter()
        tag = 1
        self.tags[tag] = None
        for edge in self.edges:
            edge.dest.assignTagsHlpr(tag, edge, c)
            tag = c.increment()

    def assignTagsHlpr(self, tag, edge, c):

        self.tags[tag] = edge.src

        if len(self.edges) == 0:
            return

        tags = [tag] + [c.increment() for i in range(1, len(self.edges))]

        for t in tags[1:]:
            self.tags[t] = edge.src
            edge.src.updateTags(tag, t)

        for edge, t in zip(self.edges, tags):
            edge.dest.assignTagsHlpr(t, edge, c)

    def updateTags(self, old_tag, new_tag):

        src = self.tags[old_tag]
        if src is None:
            self.tags[new_tag] = None
            return
        self.tags[new_tag] = src
        src.updateTags(old_tag, new_tag)

class Router(Node):

    def __init__(self, ip_address):
        super(Router, self).__init__()
        self.ip_address = ip_address

    def getPrintableID(self):
        return "Router with IP address " + str(self.ip_address)


class Switch(Node):

    def __init__(self, mac_address):
        super(Switch, self).__init__()
        self.mac_address = mac_address

    def getPrintableID(self):
        return "Switch with MAC address " + str(self.mac_address)




class Counter():
    def __init__(self):
        self.val = 1

    def increment(self):
        self.val += 1
        return self.val

g = Graph()
s1 = g.addSwitch("1")

s2 = g.addSwitch("2")
s3 = g.addSwitch("3")
s4 = g.addSwitch("4")

s5 = g.addSwitch("5")
s6 = g.addSwitch("6")

s7 = g.addSwitch("7")
s8 = g.addSwitch("8")
s9 = g.addSwitch("9")

s10 = g.addSwitch("10")

g.addEdge(s1, s2, 1)
g.addEdge(s1, s3, 1)
g.addEdge(s1, s4, 1)

g.addEdge(s2, s5, 1)
g.addEdge(s3, s5, 1)
g.addEdge(s3, s6, 1)
g.addEdge(s4, s6, 1)

g.addEdge(s5, s7, 1)
g.addEdge(s5, s8, 1)
g.addEdge(s6, s8, 1)


g.assignTags(s1)
g.printGraph()
